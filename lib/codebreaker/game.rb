module Codebreaker
  class Game
    def initialize(output)
      @output = output
    end

    def start
      @output.puts "Welcome to Codebreaker!"
      @output.puts "Enter your guess:"
    end

    def set_secret_code(code)
      @secret_code = code
    end

    def guess(guess)
      plus_marks = ''
      minus_marks = ''
      (0..3).each do |index|
        if exact_match?(guess, index)
          plus_marks << '+'
        elsif number_match?(guess, index)
          minus_marks << '-'
        end
      end
      mark = plus_marks + minus_marks
      @output.puts mark
    end

  private
    def exact_match?(guess, index)
      @secret_code[index] == guess[index]
    end
  
    def number_match?(guess, index)
      @secret_code.include?(guess[index])
    end
  end
end
