Feature: Codebreaker starts a new game

As a code-breaker
I want to be able to start a new game
So I can break a code

Scenario: Starting a new game
Given I am not playing the game
When I start the codebreaker game
Then I should see a greeting message
And I should see the message asking me to guess a secret code
