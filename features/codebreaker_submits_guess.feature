@wip
Feature: Codebreaker submits a guess

When codebreaker submits a guess, the game should reply with "+" and "-" signs. 
"+" sign indicates the exact position/number match
"-" sign indicates the number match, but not position

Scenario Outline:
  Given I have started a new game
  And the secret code is "<secret_code>"
  When the game asks me to enter my guess
  And I enter the "<guess_code>"
  Then I should see "<answer>"
  And <additional_prompt>

Scenarios: No matches
  | secret_code | guess_code | answer | additional_prompt                    |
  | 1234        | 5678       |        | I should see enter your guess again  |

Scenarios: 1 number correct
  | secret_code | guess_code | answer | additional_prompt                    |
  | 1234        | 5673       | -      | I should see enter your guess again  |
  | 1234        | 5674       | +      | I should see enter your guess again  |
  
Scenarios: 2 numbers correct
  | secret_code | guess_code | answer | additional_prompt                    |
  | 1234        | 5612       | --     | I should see enter your guess again  |
  | 1234        | 5614       | +-     | I should see enter your guess again  |
  | 1234        | 5631       | +-     | I should see enter your guess again  |
  | 1234        | 5634       | ++     | I should see enter your guess again  |

Scenarios: All numbers correct
  | secret_code | guess_code | answer | additional_prompt                    |
  | 1234        | 1234       | ++++   | I should see congratulations message |
  | 1234        | 1243       | ++--   | I should see enter your guess again  |
  | 1234        | 1423       | +---   | I should see enter your guess again  |
  | 1234        | 4321       | ----   | I should see enter your guess again  |

