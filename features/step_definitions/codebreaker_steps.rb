class Output
  def messages
    @messages ||= []
  end

  def puts(message)
    messages << message
  end
end

def output
  @output ||= Output.new
end

def game
  @game ||= Codebreaker::Game.new(output)
end


Given /^I am not playing the game$/ do
end

When /^I start the codebreaker game$/ do
  game.start
end

Then /^I should see a greeting message$/ do
  output.messages.should include("Welcome to Codebreaker!")
end

Then /^I should see the message asking me to guess a secret code$/ do
  output.messages.should include("Enter your guess:")
end

Given /^I have started a new game$/ do
  game.start
end

Given /^the secret code is "([^"]*)"$/ do |code|
  game.set_secret_code code
end

When /^the game asks me to enter my guess$/ do
  output.messages.should include("Enter your guess:")
end

When /^I enter the "([^"]*)"$/ do |guess|
  game.guess(guess)
end

Then /^I should see "([^"]*)"$/ do |message|
  output.messages.should include(message)
end

Then /^I should see enter your guess again$/ do
  output.messages.should include("Enter your guess:")
end

Then /^I should see congratulations message$/ do
  pending # express the regexp above with the code you wish you had
end

