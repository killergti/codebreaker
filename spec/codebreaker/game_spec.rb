require 'spec_helper'

module Codebreaker
  describe Game do
    let(:output) { double("output").as_null_object }
    let(:game) { Game.new(output) }

    describe "#start" do
      it "sends a welcome message" do
        output.should_receive(:puts).with("Welcome to Codebreaker!")
        game.start
      end

      it "prompts for the first guess" do
        output.should_receive(:puts).with("Enter your guess:")
        game.start
      end
    end

    describe "#guess" do
      context "with no matches" do
        it "sends a mark with ''" do
          game.start
          game.set_secret_code('1234')
          output.should_receive(:puts).with('')
          game.guess('5678')
        end
      end

      context "with 1 match" do
        it "sends a '-' sign" do
          game.start
          game.set_secret_code('1234')
          output.should_receive(:puts).with('-')
          game.guess('5671')
        end
      end
      context "with 1 exact match" do
        it "sends a '+' sign" do
          game.start
          game.set_secret_code('1234')
          output.should_receive(:puts).with('+')
          game.guess('5674')
        end
      end

      context "with 2 matches" do
        it "sends a '--' signs" do
          game.start
          game.set_secret_code('1234')
          output.should_receive(:puts).with('--')
          game.guess('5612')
        end
      end
      context "with 1 number matche and 1 exact match (in that order)" do
        it "sends a '+-' signs" do
          game.start
          game.set_secret_code('1234')
          output.should_receive(:puts).with('+-')
          game.guess('5614')
        end
      end
    end
  end
end
